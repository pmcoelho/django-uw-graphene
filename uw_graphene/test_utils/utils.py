import logging
from graphql_jwt.testcases import JSONWebTokenTestCase
from graphql_relay import to_global_id, from_global_id


def to_snake_case(value):
    return "".join(["_" + i.lower() if i.isupper() else i for i in value]).lstrip("_")


class BaseTestCase(JSONWebTokenTestCase):
    required_input = {}
    optional_input = {}
    filters = {}

    @property
    def input(self):
        return dict(**self.required_input, **self.optional_input)

    @property
    def variables(self):
        return dict(**self.filters, input=self.input)

    @property
    def query(self):
        if hasattr(self, "MUTATION"):
            return self.MUTATION
        if hasattr(self, "QUERY"):
            return self.QUERY
        return ""

    def to_global_id(self, value, model=None):
        if model is None:
            model = self.model.__class__.__name__
        return to_global_id(f"{model}", value)

    def from_global_id(self, global_id):
        return str(from_global_id(global_id)[1])

    def assert_id_matches_global_id(self, id, global_id):
        self.assertEqual(str(id), self.from_global_id(global_id))

    def assert_requires_login(self):
        self.client.logout()
        logging.disable(logging.ERROR)
        response = self.client.execute(self.query, variables=self.variables)
        logging.disable(logging.NOTSET)
        self.assertEqual(len(response.errors), 1)
        self.assertEqual(
            response.errors[0].message,
            "You do not have permission to perform this action",
        )

    def assert_response_error_contains(self, response, expected_substring):
        content = response.to_dict()
        self.assertIn(expected_substring, content["errors"][0]["message"])

    def assert_error_on_invalid_enum_value(self, key, enum_type):
        variables = dict(self.variables)
        variables["input"][key] = "this_is_an_invalid_enum_value"
        response = self.client.execute(self.query, variables=variables)
        content = response.to_dict()
        self.assertIn(f'Expected type "{enum_type}"', content["errors"][0]["message"])

    def assert_does_not_exist(self, response, field, invalid_id):
        content = response.to_dict()
        errors = content["data"][self.OP_NAME]["errors"]
        self.assertEqual(len(errors), 1)
        error = errors[0]
        self.assertEqual(error["field"], field)
        self.assertEqual(
            error["messages"][0], f"field '{field}' with id '{invalid_id}' not found"
        )

    def assert_does_not_exist__no_id_answer(self, response, field):
        """
        TODO: remove this
        This is a bad error message that should be fixed,
        meanwhile use it on needed tests
        see issue #29
        """
        content = response.to_dict()
        errors = content["data"][self.OP_NAME]["errors"]
        self.assertEqual(len(errors), 1)
        error = errors[0]
        self.assertEqual(error["field"], field)
        self.assertEqual(error["messages"][0], f"field '{field}' not found")
