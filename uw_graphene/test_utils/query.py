from .utils import BaseTestCase


class BaseQueryTestCase(BaseTestCase):
    def get_content(self):
        response = self.client.execute(self.QUERY, variables=self.variables)
        self.assertIsNone(response.errors)
        content = response.to_dict()
        return content["data"]["viewer"][self.OP_NAME]["edges"]
