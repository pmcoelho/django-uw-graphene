from datetime import date
from decimal import Decimal
from model_bakery import baker

from .utils import to_snake_case, BaseTestCase


class TestMutationMixin:
    def get_instance(self, response):
        """Gets instance from the id of the request"""
        content = response.to_dict()
        self.assertIsNone(content.get("errors"))
        self.assertIsNotNone(content.get("data"))
        self.assertIsNotNone(content.get("data").get(self.OP_NAME))
        self.assertIsNone(content.get("data").get(self.OP_NAME).get("errors"))
        return self.model.objects.get(
            pk=self.from_global_id(
                content["data"][self.OP_NAME][self.RESPONSE_FIELD]["id"]
            )
        )

    def assert_changes_on_db(self, **custom_comparers):
        """Makes sure that the expected changes are executed"""
        response = self.client.execute(self.MUTATION, variables=self.variables)
        self.assertIsNone(response.errors)

        instance = self.get_instance(response)
        for key, value in self.input.items():
            if key == "id":
                continue

            instance_value = None
            if key[-3:] == "Ids":
                instance_value = getattr(instance, to_snake_case(f"{key[:-3]}s"))
            elif not hasattr(instance, to_snake_case(key)):
                continue
            else:
                instance_value = getattr(instance, to_snake_case(key))

            if key in custom_comparers:
                custom_comparers.get(key)(instance_value, value)
                continue

            if key[-2:] == "Id":
                self.assert_id_matches_global_id(instance_value, value)
                continue

            if key[-3:] == "Ids" and isinstance(value, list):
                instance_ids = sorted([str(v.id) for v in instance_value.all()])
                ids = sorted([self.from_global_id(v) for v in value])
                for x, y in zip(instance_ids, ids):
                    self.assertEqual(x, y)
                continue

            if isinstance(instance_value, date):
                self.assertEqual(instance_value.isoformat(), value)
                continue

            if isinstance(instance_value, Decimal):
                self.assertEqual(instance_value, Decimal(str(value)))
                continue

            self.assertEqual(instance_value, value)

    def assert_field_is_required(self, field_name, field_type):
        variables = dict(self.variables)
        variables["input"].pop(field_name)
        response = self.client.execute(self.MUTATION, variables=variables)
        content = response.to_dict()
        self.assertEqual(len(content["errors"]), 1)
        self.assertIn(
            f'field "{field_name}": Expected "{field_type}!", found null.',
            content["errors"][0]["message"],
        )

    def assert_error_on_non_existing_object_from_id(self):
        self.assert_error_on_non_existing_object("id", self.model.__name__)

    def assert_error_on_mutating_not_owned_instance(self):
        self.assert_error_on_using_not_owned_reference("id", self.model)

    def assert_error_on_using_not_owned_reference(self, field_name, model_class):
        foreign_organization = baker.make("organizations.Organization")
        foreign_instance = baker.make(model_class, organization=foreign_organization)
        invalid_id = self.to_global_id(foreign_instance.id, model_class.__name__)
        variables = dict(self.variables)
        variables["input"][field_name] = invalid_id
        response = self.client.execute(self.MUTATION, variables=variables)
        self.assert_does_not_exist__no_id_answer(response, field_name)

    def assert_error_on_non_existing_object(self, field_name, model_name):
        """
        This is the same as assert_error_on_using_non_existing_reference
        but with the NoIdAnswer (temporary)
        """
        invalid_id = self.to_global_id(666, model_name)
        variables = dict(self.variables)
        variables["input"][field_name] = invalid_id
        response = self.client.execute(self.MUTATION, variables=variables)
        self.assert_does_not_exist__no_id_answer(response, field_name)

    def assert_error_on_using_non_existing_reference(self, field_name, model_name):
        invalid_id = self.to_global_id(666, model_name)
        variables = dict(**self.variables)
        variables["input"][field_name] = invalid_id
        response = self.client.execute(self.MUTATION, variables=variables)
        self.assert_does_not_exist(response, field_name, invalid_id)

    def assert_custom_error(self, error_field, error_message):
        response = self.client.execute(self.MUTATION, variables=self.variables)
        content = response.to_dict()
        self.assertIsNone(response.errors)
        errors = content["data"][self.OP_NAME]["errors"]
        self.assertEqual(len(errors), 1)
        error = errors[0]
        self.assertEqual(error["field"], error_field)
        self.assertEqual(error["messages"][0], error_message)

    def assert_field_is_unknown(self, field_name):
        self.optional_input = {field_name: "invalid_data"}
        response = self.client.execute(self.MUTATION, variables=self.variables)
        self.assert_response_error_contains(response, f'"{field_name}": Unknown field.')


class TestMutationCreateMixin(TestMutationMixin):
    def assert_field_is_set(self, expected_field, expected_value):
        """Asserts that the expected_field is set to the expected_value on the database"""
        response = self.client.execute(self.MUTATION, variables=self.variables)
        self.assertIsNone(response.errors)
        created_instance = self.get_instance(response)
        self.assertEqual(getattr(created_instance, expected_field), expected_value)


class TestMutationUpdateMixin(TestMutationMixin, BaseTestCase):
    def assert_is_set_to_null_when_not_provided(self, field_name):
        variables = dict(self.variables)
        variables["input"].pop(field_name)
        response = self.client.execute(self.MUTATION, variables=variables)
        self.assertIsNone(response.errors)
        instance = self.get_instance(response)
        if field_name[-2:] == "Id":
            field_name = field_name[:-2]
        self.assertIsNone(getattr(instance, to_snake_case(field_name)))


class TestMutationDeleteMixin(TestMutationMixin, BaseTestCase):
    def assert_changes_on_db(self, object_deleted):
        """Makes sure that the object is deleted"""
        response = self.client.execute(self.MUTATION, variables=self.variables)
        self.assertIsNone(response.errors)

        self.assertRaises(
            self.model.DoesNotExist, self.model.objects.get, id=object_deleted.id,
        )
