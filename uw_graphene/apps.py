from django.apps import AppConfig


class UwGrapheneConfig(AppConfig):
    name = 'uw_graphene'
