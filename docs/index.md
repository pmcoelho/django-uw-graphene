# Django UW Graphene Documentation

[[_TOC_]]

## Note on Authentication

For authentication I've only ever used [django-graphql-jwt](https://django-graphql-jwt.domake.io/en/latest/) with [HTTP header](https://django-graphql-jwt.domake.io/en/latest/authentication.html#http-header) method but this library should be able to handle any method as long as the user is provided on `info.context.user` mutation parameter

## Queries

An [`ExtendedConnection`](https://gitlab.com/pmcoelho/django-uw-graphene/-/blob/master/uw_graphene/connections.py#L4) was implemented to provide `totalCount` for listings:

```python
import graphene
from graphene_django.types import DjangoObjectType
from uw_graphene.connections import ExtendedConnection

from ..models import MyModel
from .filters import MyModelFilter

class MyModelNode(DjangoObjectType):
    class Meta:
        model = MyModel
        interfaces = (graphene.relay.Node, )
        connection_class = ExtendedConnection
        filterset_class = MyModelFilter
```

## Mutations
  
We decided to create an abstraction over the [`graphene.Mutation`](https://docs.graphene-python.org/projects/django/en/latest/mutations/#) to make it easier to implement CRUD mutations.

### GlobalId to Instance "auto fetching"
We noticed that every time an input referenced an Id we would need to automaticly fetch the related object so we abstracted it using [`GlobalIDMutation.fields_to_model`](#basebinmutation) and [`resolve_*`](#resolve-value-info-parent-sibilings-functions) functions

### Null value handling
Null values are accepted as input with the following rules:
  * if a value is declared with the parameter `set_null=True` and the value is not provided in the input, it will be set to null
  * if the flag `set_null` is set to false (default) the and the value is not provided, the change is ignored
  * if a value is provided as null, will be set as null

### Error Handling

All mutations return a variable called `errors` wich is a list of `{"field": "fieldName", "messages": "comma separated list with all errors for the field"}`. 
  
Some errors are handled internally when needed but can also be raised manually such as:
  * **InvalidGlobalIdError** - The provided global id was invalid or the resulting id not found
  * **InstanceNotFoundError** - The instance for the id provided was not found

the following errors and any custom error that inherits from [`MutationError`](https://gitlab.com/pmcoelho/django-uw-graphene/-/blob/master/uw_graphene/errors.py#L9)can be raised inside the `mutate_and_get_payload` function of a mutation and will automaticly be handled.
  
  * **PasswordValidationError** - Use this to raise when the password provided is invalid

### CRUD Example 
  
  ```python
import graphene
from graphql_jwt.decorators import login_required

from uw_graphene.mutations import GlobalIDMutation
from wineries.models import Winery

from .types import BinNode
from ..models import Bin


class BaseBinMutation(GlobalIDMutation):
    fields_to_model = {"id": Bin, "winery_id": Winery}

    @classmethod
    @login_required
    def resolve_id(cls, value, info, **kwargs):
        wineries = info.context.user.profile.wineries.all()
        return Bin.objects.get(pk=value, winery__in=wineries)

    @classmethod
    @login_required
    def resolve_winery_id(cls, value, info, **kwargs):
        wineries = info.context.user.profile.wineries.all()
        return Winery.objects.get(pk=value, pk__in=wineries)


class BaseBinInput(object):
    name = graphene.String(required=True)
    capacity = graphene.Float(required=True)
    winery_id = graphene.ID(required=True)


class CreateBin(BaseBinMutation):
    bin = graphene.Field(BinNode)

    class Input(BaseBinInput):
        pass

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **values):
        bin = Bin.objects.create(**values)
        return CreateBin(bin=bin)


class UpdateBin(BaseBinMutation):
    bin = graphene.Field(BinNode)

    class Input(BaseBinInput):
        id = graphene.ID(required=True)

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **values):
        instance = values.pop("_instance")
        bin = cls.update_instance(instance, values)
        return UpdateBin(bin=bin)


class DeleteBin(BaseBinMutation):
    bin = graphene.Field(BinNode)

    class Input:
        id = graphene.ID(required=True)

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **values):
        bin = values.pop("_instance")
        bin.delete()
        return DeleteBin(bin=bin)


class BinMutation(graphene.ObjectType):
    create_bin = CreateBin.Field()
    update_bin = UpdateBin.Field()
    delete_bin = DeleteBin.Field()

  ```
#### - `BaseBinMutation`
Implements shared features between all CRUD mutations.

`fields_to_model` is a mapping of the fields on the mutation input to the expected django Model, the field is only referenced in this mapping no additional filtering will be executed, but if we need custom field filters we can implement `resolve_*` functions
  
#### - `resolve_*(value, info, parent, sibilings)` functions
Use this to create custom filters for inputs.
  ```
  parameters:
    value - the value of the input
    info - the graphene-django info parameter (contains all request and user information)
    parent - this is the instance of the parent when used on nested inputs
    sibilings - input parameters already converted to instances
  ```
  Note: sibilings are executed in the same order as the input field so, if one validation depends on a sibiling pay atention to the input order
  
This can be used on nested input using `__` the same way we can with the django ORM
  
#### - `BaseBinInput`
Implements inputs shared by all CRUD mutations and is intended to be used as parent of the Mutation `class Input argument`
  
#### - `values.pop("_instance")`
The values object is transformed internally to be populated with the mappings provided on `BaseBinMutation` following this rules:
  *  input values ending in `_id` lose the prefix so `winery_id` becomes `winery`.
  *  input values ending in `_ids` lose the prefix and become a list.
  *  input value `id` is translated from GlobalId to internal id (db) and a value `_instance` is appended
  
## Test Utils (WIP)
  This module was created with the objective of making it as simple as possible to implement "e2e" tests for the api. It is still a work in progress.



